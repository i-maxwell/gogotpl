package main

import (
	"strings"
	"text/template"
)

/**
 * @description: 字段类型转换为GO类型
 * @param {string} DB字段配置
 * @return {*}	GO字段类型
 */
func ToGoType(f Fields) string {
	prefix := ""
	prefix = "*"
	if strings.HasPrefix(strings.ToUpper(f.Type), "VARCHAR") {
		if f.Serializer != "" {
			return "interface{}"
		}
		return prefix + "string"
	}
	if strings.HasPrefix(strings.ToUpper(f.Type), "TEXT") {
		if f.Serializer != "json" {
			return "interface{}"
		}
		return prefix + "string"
	}
	if strings.HasPrefix(strings.ToUpper(f.Type), "TINYINT") {
		return prefix + "int"
	}
	if strings.HasPrefix(strings.ToUpper(f.Type), "INT") {
		return prefix + "int"
	}
	if strings.HasPrefix(strings.ToUpper(f.Type), "FLOAT") {
		return prefix + "float64"
	}
	if strings.HasPrefix(strings.ToUpper(f.Type), "BIGINT") {
		return prefix + "int64"
	}
	if strings.HasPrefix(strings.ToUpper(f.Type), "DATETIME") {
		return prefix + "types.LocTime"
	}
	if strings.HasPrefix(strings.ToUpper(f.Type), "BOOL") {
		return prefix + "bool"
	}
	return "string"
}

/**
 * @description: 字段不为空，GO字段标签描述符
 * @param {bool} v 是否不为空
 * @return {*}
 */
func NotNull(v bool) string {
	if v {
		return ""
	} else {
		return "NOT NULL"
	}
}

/**
 * @description: DB结构体序列化类型
 * @param {bool} is 是否为JSON类型
 * @return {*}
 */
func Serializer(serializer string) string {
	if serializer != "" {
		return "serializer:" + serializer + ";"
	} else {
		return ""
	}
}

/**
 * @description: DB字段默认值，根据不同的字段类型，生成不同的默认值类型
 * @param {string} t
 * @return {*}
 */
func DefaultValue(t string) string {
	if strings.HasPrefix(strings.ToUpper(t), "VARCHAR") {
		return " DEFAULT ''"
	}
	if strings.HasPrefix(strings.ToUpper(t), "INT") {
		return "DEFAULT 0"
	}
	if strings.HasPrefix(strings.ToUpper(t), "BIGINT") {
		return "DEFAULT 0"
	}
	if strings.HasPrefix(strings.ToUpper(t), "FLOAT") {
		return "DEFAULT 0"
	}
	if strings.HasPrefix(strings.ToUpper(t), "BOOL") {
		return "DEFAULT false"
	}
	if strings.HasPrefix(strings.ToUpper(t), "TINYINT") {
		return "DEFAULT 0"
	}

	return ""
}

/**
 * @description: 字符串数组转换为字符串，逗号分割
 * @param {[]string} s
 * @return {*}
 */
func Join(s []string) string {
	return strings.Join(s, ",")
}

/**
 * @description: 字段自增1
 * @param {int} v
 * @return {*}
 */
func Inc(v int) int {
	return v + 1
}

/**
 * @description: 数组自动拼接前缀
 * @param {string} a	前缀字符
 * @param {[]string} b	数组
 * @return {*}
 */
func ArrayAddPrefix(a string, b []string) []string {
	rst := make([]string, 0)
	for _, v := range b {
		rst = append(rst, "`"+a+v+"`")
	}
	return rst
}

/**
 * @description: 数组转小写
 * @param {[]string} b 要转码的数组
 * @return {*}
 */
func ArrayToLower(b []string) []string {
	rst := make([]string, 0)
	for _, v := range b {
		rst = append(rst, strings.ToLower(v))
	}
	return rst
}

// 模板函数
var TplFuncs = template.FuncMap{
	"ToLower":        strings.ToLower,
	"ToDBType":       strings.ToUpper,
	"ToGoType":       ToGoType,
	"NotNull":        NotNull,
	"Serializer":     Serializer,
	"DefaultValue":   DefaultValue,
	"Join":           Join,
	"Inc":            Inc,
	"ArrayAddPrefix": ArrayAddPrefix,
	"ArrayToLower":   ArrayToLower,
}
