### 项目介绍

根据 JSON 文件，自动生成 golang 的 CURD 接口，统一标准化。同时支持自定义化开发，1 分钟完成后台增删查改需求

### 安装方式

```
go install gitee.com/i-maxwell/gogotpl@master
```

### 使用方式

##### 1. 配置模板 `vim template/article.json`

```
{
    "TableName": "Article",
    "AutoInc": 100000,
    "IndexList": [],
    "Fields": [
        {
            "Key": "Title",
            "Type": "varchar(4096)",
            "Title": "标题"
        },
        {
            "Key": "Description",
            "Type": "varchar(4096)",
            "Title": "描述"
        }
    ]
}
```

##### 2. 代码生成命令

```
gogotpl -gomod="app" -t="template" -o="./"
```

参数定义
gomod golang 服务 mod 名称，用于生成代码时候引用不同的模块
t 模板文件，默认：./tpl
o 输出路径，默认当前目录下。系统自动会生成 lib 文件夹

##### 3. 代码生成结构

```
lib
├── ao
│   └── Article.go
├── api
│   └── Article.go
├── dao
│   ├── Article.go
│   └── db.sql
├── schame
│   └── Article.go
└── web
    └── Article.vue
```

### 示例介绍

#### 一、基础用法

##### 1.插入数据

##### 2.批量插入数据

##### 3.根据 ID 查询单个数据

##### 4.根据条件查询列表

##### 5.更新数据

##### 6.删除数据

##### 7.统计数据

#### 二、高级用法

##### 1.联表查询
