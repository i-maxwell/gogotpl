package dao

import (
	"context"
	"encoding/json"
	"errors"
	"strings"

	"gitee.com/i-maxwell/go-library/db"
	"gitee.com/i-maxwell/go-library/errno"
	"gitee.com/i-maxwell/go-library/types"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

/* DB建表语句
CREATE TABLE IF NOT EXISTS [[ $.Table.TablePrefix ]][[ .Table.TableName | ToLower ]] (
`c_id` BIGINT(20) AUTO_INCREMENT NOT NULL,
[[- range .Table.Fields ]]
`[[ $.Table.ColumnPrefix ]][[ .Key | ToLower ]]` [[ .Type | ToDBType ]] [[ .IsNull | NotNull ]] [[ .Type | DefaultValue ]],
[[- end ]]
`c_createtime` datetime NOT NULL,
`c_updatetime` datetime NOT NULL,
[[- range .Table.IndexList ]]
[[ .IndexType ]]([[ .Fields | ArrayToLower | ArrayAddPrefix $.Table.ColumnPrefix | Join ]]),
[[- end ]]
PRIMARY KEY(`c_id`)
) ENGINE=InnoDB AUTO_INCREMENT=[[ .Table.AutoInc ]] DEFAULT CHARSET=utf8;
*/

// 定义枚举变量
[[- range .Table.Fields ]]
	[[- with .NumEnums ]]
var (
	[[- end ]]
	[[- $key := .Key ]]
	[[- range $idx, $v := .NumEnums ]]
		[[ $.Table.TableName ]][[ $key ]][[ index $v 0 ]] = int([[ $idx ]])
		[[- $idx = $idx | Inc ]]
	[[- end ]]
	[[- with .NumEnums ]]
)
	[[- end ]]
[[- end ]]

// 定义字段隐射
var [[ .Table.TableName ]]Columns map[string]string = map[string]string{
	"Id":                "c_id",
[[- range .Table.Fields ]]
    "[[ .Key ]]":         "[[ $.Table.ColumnPrefix ]][[ .Key | ToLower ]]",   
[[- end ]]
	"CreateTime":        "c_createtime",
	"UpdateTime":        "c_updatetime",
}

// DB结构体
type [[ .Table.TableName ]] struct {
	Id           int64 `gorm:"column:c_id;primarykey" json:"Id,omitempty"`
[[- range .Table.Fields ]]
    [[ .Key ]]    [[ . | ToGoType ]] `gorm:"column:[[ $.Table.ColumnPrefix ]][[ .Key | ToLower ]];[[ .Serializer | Serializer ]][[ .Perm ]]" json:"[[ .Key ]],omitempty"`
[[- end ]]
	CreateTime   *types.LocTime  `gorm:"column:c_createtime" json:"CreateTime,omitempty"`
	UpdateTime   *types.LocTime  `gorm:"column:c_updatetime" json:"UpdateTime,omitempty"`
	Hook         types.ModelHook `gorm:"-" json:"-"`
	Options *types.DaoOptions `gorm:"-" json:"-"`
}

/**
 * @description: 注册DB模型，用于联表查询
 * @return {*}
 */
 /*
func init() {
[[- if eq .Table.Namespace "" ]]
	db.AddModelParser("/[[ .Table.TableName ]]", &[[ .Table.TableName ]]{})
[[- else ]]
	db.AddModelParser("/[[ .Table.Namespace ]]/[[ .Table.TableName ]]", &[[ .Table.TableName ]]{})
[[- end ]]
}
*/

// 默认的全局变量
var G_[[ .Table.TableName ]] = New[[ .Table.TableName ]](&types.DaoOptions{
	IgnoreTenant: false,
	IgnoreUser: false,
	Namespace: "[[ .Table.Namespace ]]",
})

/**
 * @description: 根据参数创建表结构对象
 * @return {*}
 */
func New[[ .Table.TableName ]](op *types.DaoOptions) *[[ .Table.TableName ]]{
	return &[[ .Table.TableName ]]{Options: op}
}

/**
 * @description: DB表名称
 * @return {*}
 */
func (*[[ .Table.TableName ]]) TableName() string {
	return "t_" + strings.ToLower("[[ .Table.TableName ]]")
}

/**
 * @description: DB结构体序列化
 * @return {*}
 */
func (t *[[ .Table.TableName ]]) String() string {
	js, _ := json.Marshal(t)
	return string(js)
}

/**
 * @description: 自动填充租户信息
 * @return {*}
 */
func (t *[[ .Table.TableName ]]) SetTenant(ctx context.Context, value interface{}) {
	if t.Options != nil && t.Options.IgnoreTenant {	// 忽略租户ID的设置
		return 
	}

[[- if ne .Table.TenantId "" ]]
	c := ctx.(*types.HttpCtx)
	if data, ok := value.(*[[ .Table.TableName ]]); ok && c.TenantId > 0 {
		data.[[ .Table.TenantId ]] = &c.TenantId
	}
[[- end ]]
}

/**
 * @description: 获取结构体中，租户ID的字段
 * @return {*}
 */
func (t *[[ .Table.TableName ]]) GetTenantKey() string {
	if t.Options != nil && t.Options.IgnoreTenant {
		return ""
	}
[[- if eq .Table.TenantId "" ]]
	return ""
[[- else ]]
	return "[[ .Table.ColumnPrefix ]][[ .Table.TenantId | ToLower ]]"
[[- end ]]
}

/**
 * @description: 拼接SQL查询，填充租户信息
 * @return {*}
 */
func (t *[[ .Table.TableName ]])  PaddingTenant(ctx context.Context, where interface{}, args []interface{}) (interface{},  []interface{}) {
	c := ctx.(*types.HttpCtx)
	if wh, ok := where.(string); ok && c.TenantId > 0 && t.GetTenantKey() != "" {
		where = wh + " AND " + t.GetTenantKey() + " = ?"
		args = append(args, c.TenantId)
	}
	return where, args
}

/**
 * @description: 自动填充C端用户信息
 * @return {*}
 */
func (t *[[ .Table.TableName ]]) SetUser(ctx context.Context, value interface{}) {
	if t.Options != nil && t.Options.IgnoreUser {	// 忽略用户ID的设置
		return 
	}

[[- if ne .Table.UserId "" ]]
	c := ctx.(*types.HttpCtx)
	if data, ok := value.(*[[ .Table.TableName ]]); ok && c.UserId > 0 {
		data.[[ .Table.UserId ]] = &c.UserId
	}
[[- end ]]
}

/**
 * @description: 获取结构体中，用户ID的字段
 * @return {*}
 */
func (t *[[ .Table.TableName ]]) GetUserKey() string {
	if t.Options != nil && t.Options.IgnoreUser {
		return ""
	}
[[- if eq .Table.UserId "" ]]
	return ""
[[- else ]]
	return "[[ .Table.ColumnPrefix ]][[ .Table.UserId | ToLower ]]"
[[- end ]]
}

/**
 * @description: 拼接SQL查询，填充用户信息
 * @return {*}
 */
func (t *[[ .Table.TableName ]]) PaddingUser(ctx context.Context, where interface{}, args []interface{}) (interface{},  []interface{}) {
	c := ctx.(*types.HttpCtx)
	if wh, ok := where.(string); ok && c.UserId > 0 && t.GetUserKey() != "" {
		where = wh + " AND " + t.GetUserKey() + " = ?"
		args = append(args, c.UserId)
	}
	return where, args
}

/**
 * @description: 自动填充租户信息、用户信息字段。读写接口都会自动填写
 * @return {*}
 */
func (t *[[ .Table.TableName ]]) SetDefaultCondition(ctx context.Context, where interface{}, args []interface{}) (interface{},  []interface{}) {
	where , args = t.PaddingTenant(ctx, where, args)
	where , args = t.PaddingUser(ctx, where, args)

	t.SetTenant(ctx, where)
	t.SetUser(ctx, where)
	return where, args
}

/**
 * @description: 插入数据
 * @return {*}
 */
func (t *[[ .Table.TableName ]]) Insert(ctx context.Context, data *[[ .Table.TableName ]]) error {
	now := types.Now()
	data.CreateTime = &now
	data.UpdateTime = &now
	t.SetDefaultCondition(ctx, data, nil)

	if err := db.MustDB(context.WithValue(ctx, "DBNS", t.Options.Namespace)).Clauses(clause.Insert{Modifier: "IGNORE"}).Create(data).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description: 批量插入数据
 * @return {*}
 */
func (t *[[ .Table.TableName ]]) BatchInsert(ctx context.Context, list []*[[ .Table.TableName ]]) error {
	now := types.Now()
	for _, data := range list {
		data.CreateTime = &now
		data.UpdateTime = &now
		t.SetDefaultCondition(ctx, data, nil)
	}

	if err := db.MustDB(context.WithValue(ctx, "DBNS", t.Options.Namespace)).
		Clauses(clause.Insert{Modifier: "IGNORE"}).Create(list).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description: 删除数据
 * @return {*}
 */
func (t *[[ .Table.TableName ]]) Delete(ctx context.Context, where interface{}) error {
	t.SetDefaultCondition(ctx, where, nil)

	if err := db.MustDB(context.WithValue(ctx, "DBNS", t.Options.Namespace)).
		Where(where).Delete(&[[ .Table.TableName ]]{}).Error; err != nil {
		return err
	}
	return nil
}

/**
 * @description: 查询单个数据，不存在返回nil
 * @return {*}
 */
func (t *[[ .Table.TableName ]]) FindOne(ctx context.Context, where interface{}) (*[[ .Table.TableName ]], error) {
	t.SetDefaultCondition(ctx, where, nil)

	data := &[[ .Table.TableName ]]{}
	if err := db.MustDB(context.WithValue(ctx, "DBNS", t.Options.Namespace)).
		Model(&[[ .Table.TableName ]]{}).Where(where).Take(&data).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, err
	}

	return data, nil
}

/**
 * @description: 查询单个数据，不存在返回error
 * @return {*}
 */
func (t *[[ .Table.TableName ]]) MustFindOne(ctx context.Context, where interface{}) (*[[ .Table.TableName ]], error) {
	t.SetDefaultCondition(ctx, where, nil)

	data := &[[ .Table.TableName ]]{}
	if err := db.MustDB(context.WithValue(ctx, "DBNS", t.Options.Namespace)).
		Model(&[[ .Table.TableName ]]{}).Where(where).Take(&data).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, errno.ResourceNotExist
		}
		return nil, err
	}

	return data, nil
}

/**
 * @description: 更新数据
 * @return {*}
 */
func (t *[[ .Table.TableName ]]) Update(ctx context.Context, where interface{}, data *[[ .Table.TableName ]]) error {
	t.SetDefaultCondition(ctx, where, nil)
	
	now := types.Now()
	data.CreateTime = nil
	data.UpdateTime = &now

	if err := db.MustDB(context.WithValue(ctx, "DBNS", t.Options.Namespace)).
		Model(&[[ .Table.TableName ]]{}).Where(where).Updates(data).Error; err != nil {
		return err
	}

	return nil
}

/**
 * @description: 查询列表
 * @return {*}
 */
func  (t *[[ .Table.TableName ]]) FindList(ctx context.Context, where interface{}, args []interface{}, offset int64, limit int64, extra types.ExtraArgs) ([]*[[ .Table.TableName ]], error) {
	where , args = t.SetDefaultCondition(ctx, where, args)

	data := make([]*[[ .Table.TableName ]], 0)
	query := db.MustDB(context.WithValue(ctx, "DBNS", t.Options.Namespace)).
		Model(&[[ .Table.TableName ]]{}).Where(where, args...).Offset(int(offset)).Limit(int(limit))
	if extra.Group != "" {
		query.Group(extra.Group)
	}
	if extra.Select != nil {
		query.Select(extra.Select)
	} else {
		query.Select("*")
	}
	query.Order(extra.Order)
	if err := query.Find(&data).Error; err != nil {
		return nil, err
	}
	return data, nil
}

/**
 * @description: 查询数据个数
 * @return {*}
 */
func (t *[[ .Table.TableName ]]) Count(ctx context.Context, where interface{}, args []interface{}) (int64, error) {
	where , args = t.SetDefaultCondition(ctx, where, args)

	data := int64(0)
	if err := db.MustDB(context.WithValue(ctx, "DBNS", t.Options.Namespace)).
		Model(&[[ .Table.TableName ]]{}).Where(where, args...).Count(&data).Error; err != nil {
		return 0, err
	}
	return data, nil
}

/**
 * @description: 统计数据信息
 * @return {*}
 */
func (t *[[ .Table.TableName ]]) Statistics(ctx context.Context, where interface{}, args []interface{}, extra types.ExtraArgs) ([]*types.StatisticsTotal, error) {
	where , args = t.SetDefaultCondition(ctx, where, args)

	data := make([]*types.StatisticsTotal, 0)
	query := db.MustDB(context.WithValue(ctx, "DBNS", t.Options.Namespace)).
		Model(&[[ .Table.TableName ]]{}).Select(extra.Select).Where(where, args...).Order(extra.Order)
	if extra.Group != "" {
		query.Group(extra.Group)
	}
	if err := query.Find(&data).Error; err != nil {
		return nil, err
	}
	return data, nil
}

/**
 * @description: 获取DB句柄
 * @return {*}
 */
func (t *[[ .Table.TableName ]]) GetDB() *gorm.DB {	
	query := db.MustDB(context.WithValue(context.Background(), "DBNS", t.Options.Namespace))
	return query
}

/**
 * @description: 拼接查询SQL，用于联表查询的子SQL
 * @return {*}
 */
func (t *[[ .Table.TableName ]]) SubSQL(where interface{}, args []interface{}, extra types.ExtraArgs) (*gorm.DB, error) {	
	query := db.MustDB(context.WithValue(context.Background(), "DBNS", t.Options.Namespace)).
		Model(&[[ .Table.TableName ]]{}).Select(extra.Select).Where(where, args...)
	return query, nil
}

/**
 * @description: 根据KEY获取DB字段的id
 * @return {*}
 */
func (t *[[ .Table.TableName ]]) GetColumnKey(key string) string {
	k, ok := [[ .Table.TableName ]]Columns[key]
	if ok {
		return k
	}
	return ""
}

