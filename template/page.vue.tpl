<template>
  <div class="table-container">
    <i-page-list
      v-model="queryForm"
      :list="list"
      :total="total"
      :querys="querys"
      :actions="actions"
      :columns="columns"
      :height="height"
      :list-loading="listLoading"
      @fetch-data="fetchData"
    ></i-page-list>
  </div>
</template>

<script>
  export default {
    name: 'ComprehensiveTable',
    components: {
    },
    data() {
      return {
        list: [],
        listLoading: true,
        total: 0,

        queryForm: {
          pageNo: 1,
          pageSize: 20,
        },

        querys: [
[[- range .Table.Fields ]]
            { label: '[[ .Title ]]', key: '[[ .Key ]]' },
[[- end ]]
        ],
        actions: [
          {
            label: '添加',
            icon: 'el-icon-plus',
            handler: () => this.$refs['edit'].show(),
          },
        ],
        columns: [
          { label: '序号', component: 'i-table-index' },
[[- range .Table.Fields ]]
            { label: '[[ .Title ]]', prop: '[[ .Key ]]' },
[[- end ]]
          { label: '时间', prop: 'UpdateTime' },
          {
            label: '操作',
            ops: [
              {
                label: '编辑',
                handler: (row) => this.$refs['edit'].show(row),
              },
              { label: '删除', handler: this.handleDelete },
            ],
            component: 'i-table-operate',
          },
        ],
        roles: [],
      }
    },
    computed: {
      height() {
        return this.$baseTableHeight()
      },
    },
    created() {
      this.fetchData()
    },
    methods: {
      handleDelete(row) {
        if (row.Id) {
          this.$baseConfirm('你确定要删除当前项吗', null, async () => {
            const { msg } = await this.post('/[[ .Table.TableName | ToLower ]]/delete', { Id: row.Id })
            this.$baseMessage(msg, 'success')
            this.fetchData()
          })
        } else {
          if (this.selectRows.length > 0) {
            const ids = this.selectRows.map((item) => item.id).join()
            this.$baseConfirm('你确定要删除选中项吗', null, async () => {
              const { msg } = await doDelete({ ids: ids })
              this.$baseMessage(msg, 'success')
              this.fetchData()
            })
          } else {
            this.$baseMessage('未选中任何行', 'error')
            return false
          }
        }
      },
      async fetchData() {
        this.listLoading = true
        const response = await this.post('/[[ .Table.TableName | ToLower ]]/list', {
          Limit: this.queryForm.pageSize,
          Offset: (this.queryForm.pageNo - 1) * this.queryForm.pageSize,
          Filters: [
[[- range .Table.Fields ]]
            { Key: '[[ .Key ]]',Values: this.queryForm?.[[ .Key ]],},
[[- end ]]
          ],
        })
        const { List, Total } = response.data
    
        this.list = List
        this.total = Total
        setTimeout(() => {
          this.listLoading = false
        }, 500)
      },
    },
  }
</script>
