package define

import (
	"[[ .ModName ]]/lib/dao"

	"gitee.com/i-maxwell/go-library/types"
)

// 批量插入接口
type BatchAdd[[ .Table.TableName ]]Request struct {
	List []*dao.[[ .Table.TableName ]] `json:"List"`
}
type BatchAdd[[ .Table.TableName ]]Response struct{}

// 单个插入接口，返回自增唯一ID
type Add[[ .Table.TableName ]]Request dao.[[ .Table.TableName ]]
type Add[[ .Table.TableName ]]Response types.ModelId

// 删除数据
type Delete[[ .Table.TableName ]]Request types.ModelId
type Delete[[ .Table.TableName ]]Response types.ModelId

// 查询单个信息
type Get[[ .Table.TableName ]]Request types.ModelId
type Get[[ .Table.TableName ]]Response dao.[[ .Table.TableName ]]

// 查询列表，可联表查询
type List[[ .Table.TableName ]]Request types.ListRequest
type List[[ .Table.TableName ]]Response types.ListResponse

// 查询统计信息
type Statistics[[ .Table.TableName ]]Request types.ListRequest
type Statistics[[ .Table.TableName ]]Response types.StatisticsResponse

// 更新数据
type Update[[ .Table.TableName ]]Request dao.[[ .Table.TableName ]]
type Update[[ .Table.TableName ]]Response types.ModelId
