package api

import (
	"context"
	"errors"
	"[[ .ModName ]]/lib/define"
	"[[ .ModName ]]/lib/ao"

	"gitee.com/i-maxwell/go-library/validate"
	"gitee.com/i-maxwell/go-library/route"
	"github.com/gin-gonic/gin"
)

// API接口列表，info: 根据ID查询单个数据, batchadd: 批量插入接口, add: 单个插入接口, update: 根据ID更新数据, delete: 根据ID删除接口, list: 查询数据列表，可联表查询, statistics: 统计数据信息
var [[ .Table.TableName ]]Actions = []*route.Action{
	{Action: "/[[ .Table.TableName | ToLower ]]/info", Title: "查询[[ .Table.Title ]]", Handler: Get[[ .Table.TableName ]]Api, AllowAnony: [[ .Table.AllowAnony ]]},
	{Action: "/[[ .Table.TableName | ToLower ]]/batchadd", Title: "批量添加[[ .Table.Title ]]", Handler: BatchAdd[[ .Table.TableName ]]Api, AllowAnony: [[ .Table.AllowAnony ]]},
	{Action: "/[[ .Table.TableName | ToLower ]]/add", Title: "添加[[ .Table.Title ]]", Handler: Add[[ .Table.TableName ]]Api, AllowAnony: [[ .Table.AllowAnony ]]},
	{Action: "/[[ .Table.TableName | ToLower ]]/update", Title: "修改[[ .Table.Title ]]", Handler: Update[[ .Table.TableName ]]Api, AllowAnony: [[ .Table.AllowAnony ]]},
	{Action: "/[[ .Table.TableName | ToLower ]]/delete", Title: "删除[[ .Table.Title ]]", Handler: Delete[[ .Table.TableName ]]Api, AllowAnony: [[ .Table.AllowAnony ]]},
	{Action: "/[[ .Table.TableName | ToLower ]]/list", Title: "查询[[ .Table.Title ]]列表", Handler: List[[ .Table.TableName ]]Api, AllowAnony: [[ .Table.AllowAnony ]]},
	{Action: "/[[ .Table.TableName | ToLower ]]/statistics", Title: "统计[[ .Table.Title ]]信息", Handler: Statistics[[ .Table.TableName ]]Api, AllowAnony: [[ .Table.AllowAnony ]]},
}

/**
 * @description: 批量插入接口
 * @return {*}
 */
func BatchAdd[[ .Table.TableName ]]Api(ctx context.Context, c *gin.Context) (interface{}, error) {
	req := &define.BatchAdd[[ .Table.TableName ]]Request{}
	if err := validate.CheckParams(c, req); err != nil {
		return nil, errors.New("paramters error:" + err.Error())
	}

	return ao.G_[[ .Table.TableName ]].BatchAdd(ctx, req)
}

/**
 * @description: 单个插入接口
 * @return {*}
 */
func Add[[ .Table.TableName ]]Api(ctx context.Context, c *gin.Context) (interface{}, error) {
	req := &define.Add[[ .Table.TableName ]]Request{}
	if err := validate.CheckParams(c, req); err != nil {
		return nil, errors.New("paramters error:" + err.Error())
	}

	return ao.G_[[ .Table.TableName ]].Add(ctx, req)
}

/**
 * @description: 查询单个信息
 * @return {*}
 */
func Get[[ .Table.TableName ]]Api(ctx context.Context, c *gin.Context) (interface{}, error) {
	req := &define.Get[[ .Table.TableName ]]Request{}
	if err := validate.CheckParams(c, req); err != nil {
		return nil, errors.New("paramters error:" + err.Error())
	}

	return ao.G_[[ .Table.TableName ]].Get(ctx, req)
}

/**
 * @description: 查询列表，可联表查询
 * @return {*}
 */
func List[[ .Table.TableName ]]Api(ctx context.Context, c *gin.Context) (interface{}, error) {
	req := &define.List[[ .Table.TableName ]]Request{}
	if err := validate.CheckParams(c, req); err != nil {
		return nil, errors.New("paramters error:" + err.Error())
	}

	return ao.G_[[ .Table.TableName ]].List(ctx, req)
}

/**
 * @description: 统计数据信息
 * @return {*}
 */
func Statistics[[ .Table.TableName ]]Api(ctx context.Context, c *gin.Context) (interface{}, error) {
	req := &define.Statistics[[ .Table.TableName ]]Request{}
	if err := validate.CheckParams(c, req); err != nil {
		return nil, errors.New("paramters error:" + err.Error())
	}

	return ao.G_[[ .Table.TableName ]].Statistics(ctx, req)
}

/**
 * @description: 删除数据
 * @return {*}
 */
func Delete[[ .Table.TableName ]]Api(ctx context.Context, c *gin.Context) (interface{}, error) {
	req := &define.Delete[[ .Table.TableName ]]Request{}
	if err := validate.CheckParams(c, req); err != nil {
		return nil, errors.New("paramters error:" + err.Error())
	}

	return ao.G_[[ .Table.TableName ]].Delete(ctx, req)
}

/**
 * @description: 更新数据
 * @return {*}
 */
func Update[[ .Table.TableName ]]Api(ctx context.Context, c *gin.Context) (interface{}, error) {
	req := &define.Update[[ .Table.TableName ]]Request{}
	if err := validate.CheckParams(c, req); err != nil {
		return nil, errors.New("paramters error:" + err.Error())
	}

	return ao.G_[[ .Table.TableName ]].Update(ctx, req)
}
