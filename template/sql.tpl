

CREATE TABLE IF NOT EXISTS [[ $.Table.TablePrefix ]][[ .Table.TableName | ToLower ]] (
`c_id` BIGINT(20) AUTO_INCREMENT NOT NULL,
[[- range .Table.Fields ]]
`[[ $.Table.ColumnPrefix ]][[ .Key | ToLower ]]` [[ .Type | ToDBType ]] [[ .IsNull | NotNull ]] [[ .Type | DefaultValue ]],
[[- end ]]
`c_createtime` datetime NOT NULL,
`c_updatetime` datetime NOT NULL,
[[- range .Table.IndexList ]]
[[ .IndexType ]]([[ .Fields | ArrayToLower | ArrayAddPrefix $.Table.ColumnPrefix | Join ]]),
[[- end ]]
PRIMARY KEY(`c_id`)
) ENGINE=InnoDB AUTO_INCREMENT=[[ .Table.AutoInc ]] DEFAULT CHARSET=utf8;

