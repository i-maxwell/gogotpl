package ao

import (
	"context"
	"fmt"
	"strings"
	"[[ .ModName ]]/lib/dao"
	"[[ .ModName ]]/lib/define"

	"gorm.io/gorm"
	"gitee.com/i-maxwell/go-library/db"
	"gitee.com/i-maxwell/go-library/types"
	"gitee.com/i-maxwell/go-library/utils"
)

// 结构体
type [[ .Table.TableName ]] struct {}

// 结构体实例
var G_[[ .Table.TableName ]] = [[ .Table.TableName ]]{}

// 注册AO结构体实例
func init() {
[[- if eq .Table.Namespace "" ]]
	db.AddModelParser("[[ .Table.TableName ]]", &[[ .Table.TableName ]]{})
[[- else ]]
	db.AddModelParser("/[[ .Table.Namespace ]]/[[ .Table.TableName ]]", &[[ .Table.TableName ]]{})
[[- end ]]
}

/**
 * @description: 批量插入数据
 * @return {*}
 */
func (a *[[ .Table.TableName ]]) BatchAdd(ctx context.Context, req *define.BatchAdd[[ .Table.TableName ]]Request) (*define.BatchAdd[[ .Table.TableName ]]Response, error) {

	if len(req.List) == 0 {
		return &define.BatchAdd[[ .Table.TableName ]]Response{}, nil
	}
	for _, v := range req.List {
		v.Id = 0
	}

	if dao.G_[[ .Table.TableName ]].Hook.BatchAddBefore != nil {
		data, err := dao.G_[[ .Table.TableName ]].Hook.BatchAddBefore(ctx, req.List)
		if err != nil {
			return nil, err
		}
		req.List = data.([]*dao.[[ .Table.TableName ]])
	}
	if err := dao.G_[[ .Table.TableName ]].BatchInsert(ctx, req.List); err != nil {
		return nil, err
	}

	return &define.BatchAdd[[ .Table.TableName ]]Response{}, nil
}

/**
 * @description: 插入单个数据
 * @return {*}
 */
func (a *[[ .Table.TableName ]]) Add(ctx context.Context, req *define.Add[[ .Table.TableName ]]Request) (*define.Add[[ .Table.TableName ]]Response, error) {
	req.Id = 0

	data := (*dao.[[ .Table.TableName ]])(req)
	if dao.G_[[ .Table.TableName ]].Hook.AddBefore != nil {
		tdata, err := dao.G_[[ .Table.TableName ]].Hook.AddBefore(ctx, data)
		if err != nil {
			return nil, err
		}
		data = tdata.(*dao.[[ .Table.TableName ]])
	}

	if err := dao.G_[[ .Table.TableName ]].Insert(ctx, data); err != nil {
		return nil, err
	}

	if dao.G_[[ .Table.TableName ]].Hook.AddAfter != nil {
		dao.G_[[ .Table.TableName ]].Hook.AddAfter(ctx, data)
	}

	return &define.Add[[ .Table.TableName ]]Response{
		Id: req.Id,
	}, nil
}

/**
 * @description: 删除数据，根据唯一ID删除
 * @return {*}
 */
func (a *[[ .Table.TableName ]]) Delete(ctx context.Context, req *define.Delete[[ .Table.TableName ]]Request) (*define.Delete[[ .Table.TableName ]]Response, error) {

	where := &dao.[[ .Table.TableName ]]{
		Id: req.Id,
	}

	if err := dao.G_[[ .Table.TableName ]].Delete(ctx, where); err != nil {
		return nil, err
	}

	if dao.G_[[ .Table.TableName ]].Hook.DeleteAfter != nil {
		dao.G_[[ .Table.TableName ]].Hook.DeleteAfter(ctx, where)
	}

	return &define.Delete[[ .Table.TableName ]]Response{}, nil
}

/**
 * @description: 更新数据，根据唯一ID更新
 * @return {*}
 */
func (a *[[ .Table.TableName ]]) Update(ctx context.Context, req *define.Update[[ .Table.TableName ]]Request) (*define.Update[[ .Table.TableName ]]Response, error) {

	where := &dao.[[ .Table.TableName ]]{
		Id: req.Id,
	}
	data := (*dao.[[ .Table.TableName ]])(req)

	if dao.G_[[ .Table.TableName ]].Hook.UpdateBefore != nil {
		tw, td, err := dao.G_[[ .Table.TableName ]].Hook.UpdateBefore(ctx, where, data)
		if err != nil {
			return nil, err
		}
		where = tw.(*dao.[[ .Table.TableName ]])
		data = td.(*dao.[[ .Table.TableName ]])
	}
	if err := dao.G_[[ .Table.TableName ]].Update(ctx, where, data); err != nil {
		return nil, err
	}

	if dao.G_[[ .Table.TableName ]].Hook.UpdateAfter != nil {
		dao.G_[[ .Table.TableName ]].Hook.UpdateAfter(ctx, where, data)
	}

	return &define.Update[[ .Table.TableName ]]Response{}, nil
}

/**
 * @description: 查询单个数据，根据ID查询
 * @return {*}
 */
func (a *[[ .Table.TableName ]]) Get(ctx context.Context, req *define.Get[[ .Table.TableName ]]Request) (*define.Get[[ .Table.TableName ]]Response, error) {

	where := &dao.[[ .Table.TableName ]]{
		Id: req.Id,
	}

	data, err := dao.G_[[ .Table.TableName ]].FindOne(ctx, where)
	if err != nil {
		return nil, err
	}

	if dao.G_[[ .Table.TableName ]].Hook.GetInfoAfter != nil {
		_, err := dao.G_[[ .Table.TableName ]].Hook.GetInfoAfter(ctx, data)
		if err != nil {
			return nil, err
		}
	}

	return (*define.Get[[ .Table.TableName ]]Response)(data), nil
}

/**
 * @description: 查询数据列表，根据条件查询，了联表操作
 * @return {*}
 */
func (a *[[ .Table.TableName ]]) List(ctx context.Context, req *define.List[[ .Table.TableName ]]Request) (*define.List[[ .Table.TableName ]]Response, error) {
	where, args := a.ParseWhere(req.Filters, []string{})

	_, group := a.ParseGroups(req.Groups, false)
	ext := types.ExtraArgs {
		Select: a.ParseSelect(req.Fields, false),
		Group: group,
		Order: a.ParseOrder(req.Orders),
	}

	if dao.G_[[ .Table.TableName ]].Hook.GetListBefore != nil {
		twhere, targs, text, err := dao.G_[[ .Table.TableName ]].Hook.GetListBefore(ctx, where, args, ext)
		if err != nil {
			return nil, err
		}
		where = twhere.(string)
		args = targs.([]interface{})
		ext = text.(types.ExtraArgs)
	}

	list, err := dao.G_[[ .Table.TableName ]].FindList(ctx, where, args, req.Offset, req.Limit, ext)
	if err != nil {
		return nil, err
	}

	if dao.G_[[ .Table.TableName ]].Hook.GetListAfter != nil {
		_, err := dao.G_[[ .Table.TableName ]].Hook.GetListAfter(ctx, list)
		if err != nil {
			return nil, err
		}
	}
	total, err := dao.G_[[ .Table.TableName ]].Count(ctx, where, args)
	if err != nil {
		return nil, err
	}
	return &define.List[[ .Table.TableName ]]Response{
		Total: total,
		List:  list,
	}, nil
}

/**
 * @description: 数据统计接口
 * @return {*}
 */
func (a *[[ .Table.TableName ]]) Statistics(ctx context.Context, req *define.Statistics[[ .Table.TableName ]]Request) (*define.Statistics[[ .Table.TableName ]]Response, error) {
	
	field, group := a.ParseGroups(req.Groups, true)
	ext := types.ExtraArgs {
		Select: append(a.ParseSelect(req.Fields, true), field...),
		Group: group,
		Order: a.ParseOrder(req.Orders),
	}

	where, args := a.ParseWhere(req.Filters, []string{})
	data, err := dao.G_[[ .Table.TableName ]].Statistics(ctx, where, args, ext)
	if err != nil {
		return nil, err
	}

	return &define.Statistics[[ .Table.TableName ]]Response{
		List: data,
	}, nil
}

/**
 * @description: 解析列表查询条件信息
 * @return {*}
 */
func (a *[[ .Table.TableName ]]) ParseWhere(filters []types.Filter, ignore []string) (string, []interface{}) {
	where := "1 "
	args := make([]interface{}, 0)
	for _, v := range filters {
		k := dao.G_[[ .Table.TableName ]].GetColumnKey(v.Key)
		if k == "" || (v.Values == "" && v.SubSQL == nil) {
			continue
		}
		if find, _ := utils.InArray(ignore, v.Key); find {
			continue
		}
		var val interface{}
		if v.Operate == "" {
			v.Operate = "IN"
		}
		if v.IsSearch {
			v.Operate = "LIKE"
			val = "%"+v.Values+"%"
		} else if v.SubSQL != nil {
			name := v.SubSQL.Model
			if name[0] != '/' {
				name = "/[[ .Table.Namespace ]]/" + name
			}
			module := db.FindModelParser(name)
			val = module.SubSQL(v.SubSQL, []string{})
			if val == nil {
				continue
			}
		} else {
			val = strings.Split(v.Values, ",")
		}
		if ok, err := utils.InArray([]string{">", "<", ">=", "<=", "!=", "<>", "LIKE", "IN", "NOT IN"}, v.Operate); err == nil && ok {
			where += fmt.Sprintf(" AND %s %s (?)", k, v.Operate)
			args = append(args, val)
		}
	}

	return where, args
}

/**
 * @description: 解析查询字段列表，如果是统计接口会全部as为通用的名称，如total1、total2
 * @return {*}
 */
func (a *[[ .Table.TableName ]]) ParseSelect(fields []types.SelectField, isStatistics bool) []string {
	selectList := make([]string, 0)
	for i, v := range fields {
		alias := v.Alias
		if isStatistics && alias == "" {	// 统计接口，默认total + ${index}
			alias = fmt.Sprintf("c_total%d", i)
		}
		key := v.Key
		if k := dao.G_[[ .Table.TableName ]].GetColumnKey(v.Key); k != "" {
			if v.Operate != "" {
				key = fmt.Sprintf("%s(%s)", v.Operate, k)
			} else {
				key = k
			}
 		}
		if alias != "" {
			selectList = append(selectList, fmt.Sprintf(" %s AS %s", key, alias))
		} else {
			selectList = append(selectList, fmt.Sprintf(" %s", key))
		}
 	}
	if len(selectList) == 0 {
		selectList = append(selectList, "*")
	}

	return selectList
}

/**
 * @description: 解析统计维度信息
 * @return {*}
 */
func (a *[[ .Table.TableName ]]) ParseGroups(fields []types.SelectField, isStatistics bool) ([]string, string) {
	selects := make([]string, 0)
	groups := make([]string, 0)
	for i, v := range fields {
		alias := v.Alias
		if isStatistics && alias == "" {	// 统计接口，默认total + ${index}
			alias = fmt.Sprintf("c_key%d", i)
		}

		key := v.Key
		if k := dao.G_[[ .Table.TableName ]].GetColumnKey(v.Key); k != "" {
			key = k
			if alias == "" { // 默认为DB字段名称
				alias = k
			}
		}
		selects = append(selects, fmt.Sprintf("%s AS %s", key, alias))
		groups = append(groups, alias)
	}
	return selects, strings.Join(groups, ",")
}

/**
 * @description: 解析批量查询排序字段
 * @return {*}
 */
func (a *[[ .Table.TableName ]]) ParseOrder(orders []types.Order) string {
	orderby := make([]string, 0)

	for _, order := range orders {
		key := order.Key
		if k := dao.G_[[ .Table.TableName ]].GetColumnKey(order.Key); k != "" {
			key = k
		}
		orderby = append(orderby, fmt.Sprintf("%s %s", key, order.Value))
	}
	
	return strings.Join(orderby, ",")
}

/**
 * @description: 联表子查询
 * @return {*}
 */
func (a *[[ .Table.TableName ]]) SubSQL(subSQL *types.SQLFilter, ignore []string) *gorm.DB {
	if len(subSQL.Filters) == 0 {
		return nil
	}

	wh, args := a.ParseWhere(subSQL.Filters, ignore)
	val, err := dao.G_[[ .Table.TableName ]].SubSQL(wh, args, types.ExtraArgs{
		Select: dao.G_[[ .Table.TableName ]].GetColumnKey(subSQL.Field),
	})
	if err != nil {
		return nil
	}

	return val
}