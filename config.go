package main

type MysqlType string

/**
<- 设置字段写入的权限， <-:create 只创建、<-:update 只更新、<-:false 无写入权限、<- 创建和更新权限
-> 设置字段读的权限，->:false 无读权限
*/
type Fields struct {
	Key        string     `json:"Key"`        // DB 字段
	Type       string     `json:"Type"`       // DB字段类型
	Title      string     `json:"Title"`      // DB字段标题
	Serializer string     `json:"Serializer"` // 序列化类型
	Perm       string     `json:"Perm"`       // 字段读写权限
	IsNull     bool       `json:"NotNull"`    // 是否可以为空
	Enums      [][]string `json:"Enums"`      // 字段的数字枚举值
	NumEnums   [][]string `json:"NumEnums"`   // 字段的数字枚举值
}

type MysqlIndex struct {
	IndexType string   `json:"IndexType"` // 索引类型
	Fields    []string `json:"Fields"`    // 索引字段列表
}

type TableTemp struct {
	Namespace    string       `json:"Namespace"`    // 模块名称
	TableName    string       `json:"TableName"`    // 表名
	Title        string       `json:"Title"`        // 表标题
	TablePrefix  string       `json:"TablePrefix"`  // 表前缀
	ColumnPrefix string       `json:"ColumnPrefix"` // 字段前缀
	TenantId     string       `json:"TenantId"`     // 租户ID的字段
	UserId       string       `json:"UserId"`       // 客户ID的字段
	AutoInc      int64        `json:"AutoInc"`      // 指定唯一ID开始值
	AllowAnony   bool         `json:"AllowAnony"`   // 生成的CURD接口，是否可以匿名访问
	IndexList    []MysqlIndex `json:"IndexList"`    // 索引列表
	Fields       []Fields     `json:"Fields"`       // 字段
}

type Template struct {
	Table   *TableTemp `json:"Table"`   // 表结构
	ModName string     `json:"ModName"` // gomod 名字
}
