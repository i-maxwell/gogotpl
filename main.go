package main

import (
	_ "embed"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"text/template"
)

// API模板文件
//go:embed template/api.go.tpl
var ApiTplText string

// Ao逻辑层模板文件
//go:embed template/ao.go.tpl
var AoTplText string

// Dao数据模型层模板文件
//go:embed template/dao.go.tpl
var DaoTplText string

// Schame数据结构模板文件
//go:embed template/define.go.tpl
var DefineTplText string

// 前端VUE 表格模板文件
//go:embed template/page.vue.tpl
var TableTplText string

// SQL建表语句模板文件，基于Mysql
//go:embed template/sql.tpl
var SQLTplText string

// 模板文件列表
var TplFiles = map[string]string{
	"/lib/api/${TableName}.go":    ApiTplText,
	"/lib/ao/${TableName}.go":     AoTplText,
	"/lib/dao/${TableName}.go":    DaoTplText,
	"/lib/define/${TableName}.go": DefineTplText,
	"/lib/web/${TableName}.vue":   TableTplText,
}

/**
 * @description: 输入SQL建表语句
 * @param {string} outfile
 * @param {*Template} tmp
 * @return {*}
 */
func OutputSQL(tmp *Template, outfile string) {
	f, err := os.OpenFile(outfile, os.O_WRONLY|os.O_APPEND, 0644)
	if err != nil {
		panic(err)
	}

	t, err := template.New(filepath.Base("sql.tpl")).Delims("[[", "]]").Funcs(TplFuncs).Parse(SQLTplText)
	if err != nil {
		fmt.Printf("%+v\n", tmp)
		panic(err)
	}

	err = t.Funcs(TplFuncs).Execute(f, tmp)
	if err != nil {
		fmt.Printf("%+v\n", tmp.Table)
		panic(err)
	}

}

func OutputTemplate(tmp *Template, tpl string, out, file string) {
	t, err := template.New(filepath.Base(tpl)).Delims("[[", "]]").Funcs(TplFuncs).Parse(tpl)
	if err != nil {
		fmt.Printf("%+v\n", tmp)
		panic(err)
	}

	outfile := out + file
	outfile = strings.ReplaceAll(outfile, "${TableName}", tmp.Table.TableName)
	os.MkdirAll(filepath.Dir(outfile), 0755)
	f, err := os.OpenFile(outfile, os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0644)
	if err != nil {
		panic(err)
	}

	err = t.Funcs(TplFuncs).Execute(f, tmp)
	if err != nil {
		fmt.Printf("%+v\n", tmp.Table)
		panic(err)
	}
}

var (
	// 模板路径
	path *string
	// 输出目录
	out *string
	// gomod
	mod *string
)

func init() {
	path = flag.String("t", "./ddl", "path")
	out = flag.String("o", "./", "output path")
	mod = flag.String("gomod", "App", "golang mod name")
}

func outputLog() {
	fmt.Printf("------------------start------------------\n")
	fmt.Printf("%20s %s\n", "gomod name:", *mod)
	fmt.Printf("%20s %s\n", "template path:", *path)
	fmt.Printf("%20s %s\n", "output path:", *out)
}

func main() {
	flag.Parse()
	outputLog()

	fileInfoList, err := ioutil.ReadDir(*path)
	if err != nil {
		panic(err)
	}

	if len(fileInfoList) == 0 {
		panic("no tpl file")
	}
	tmp := &Template{
		ModName: *mod,
	}

	outfile := *out + "/lib/dao/db.sql"
	os.MkdirAll(filepath.Dir(outfile), 0755)
	os.OpenFile(outfile, os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0644)
	for _, v := range fileInfoList {
		if !strings.HasSuffix(v.Name(), ".json") {
			continue
		}
		data, err := ioutil.ReadFile(*path + "/" + v.Name())
		if err != nil {
			panic(err)
		}
		tbl := &TableTemp{}
		json.Unmarshal(data, tbl)
		tmp.Table = tbl

		fmt.Printf("%20s %s -> %s\n", "parse:", tbl.TableName, v.Name())
		for file, tpltext := range TplFiles {
			OutputTemplate(tmp, tpltext, *out, file)
		}

		OutputSQL(tmp, outfile)
	}
}
